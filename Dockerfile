FROM node

RUN mkdir /muso
WORKDIR /muso
COPY package.json /muso
RUN yarn install

COPY . /muso


RUN yarn test
RUN yarn build

CMD yarn start


EXPOSE 3000 
